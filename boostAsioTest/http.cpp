#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>
#include <string>
#include <boost/asio.hpp>
#include <string>
#include <regex>

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "rus");
	
	std::string host, filePath;

		if (argc >= 3)
		{
			host = argv[1];
			filePath = argv[2];
		}
		else {
			host = "olymp.i-exam.ru/";
			filePath = "/";
		}

		boost::asio::io_service io_service;
		boost::asio::ip::tcp::resolver resolver(io_service);
		boost::asio::ip::tcp::resolver::query query(host, "http");
		boost::asio::ip::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
		boost::system::error_code error;

		boost::asio::ip::tcp::socket socket(io_service);

		socket.connect(*endpoint_iterator, error);

		if (error)
			throw boost::system::system_error(error);

		boost::asio::streambuf request;
		boost::asio::streambuf response;
		std::ostream request_stream(&request);

		request_stream
			<< "GET " + filePath + " HTTP/1.0\r\n"
			<< "Host: " + host + "\r\n"
			<< "Accept: text/html\r\n"
			<< "Connection: close\r\n\r\n";

		boost::asio::write(socket, request);
		boost::asio::read_until(socket, response, "\r\n");

		while (boost::asio::read(socket, response, boost::asio::transfer_at_least(1), error))
		{
			std::cout << &response;
		}

		system("pause");
}