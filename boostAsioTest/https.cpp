#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>
#include <string>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/bind.hpp>

extern "C" { FILE __iob_func[3] = { *stdin,*stdout,*stderr }; }
#include <stdio.h>
#pragma comment(lib, "legacy_stdio_definitions.lib")
#pragma comment (lib, "ws2_32.lib")
#pragma comment (lib, "crypt32.lib")

using boost::asio::ip::tcp;
namespace ssl = boost::asio::ssl;
typedef ssl::stream<tcp::socket> ssl_socket;

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "rus");
	try
	{
		std::string host, path;

		if (argc >= 3)
		{
			host = argv[1];
			path = argv[2];
		}
		else {
			host = "vk.com";
			path = "/";
		}

		boost::asio::io_service io_service;

		ssl::context ctx(ssl::context::sslv23);
		ctx.set_default_verify_paths();

		tcp::resolver resolver(io_service);
		tcp::resolver::query query("vk.com", "https");
		tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

		ssl_socket socket(io_service, ctx);
		boost::asio::connect(socket.lowest_layer(), endpoint_iterator);
		socket.lowest_layer().set_option(tcp::no_delay(true));

		socket.set_verify_mode(boost::asio::ssl::verify_none); //verify_peer to check cert
		//socket.set_verify_callback(ssl::rfc2818_verification(host));
		socket.handshake(ssl_socket::client);

		boost::asio::streambuf request;
		std::ostream request_stream(&request);
		request_stream
			<< "GET " << path << " HTTP/1.0\r\n"
			<< "Host: " << host << "\r\n"
			<< "Accept: */*\r\n"
			<< "Connection: close\r\n\r\n";

		boost::asio::write(socket, request);

		boost::asio::streambuf response;
		boost::asio::read_until(socket, response, "\r\n");

		// Check that response is OK.
		std::istream response_stream(&response);
		std::string http_version;
		response_stream >> http_version;
		unsigned int status_code;
		response_stream >> status_code;
		std::string status_message;
		std::getline(response_stream, status_message);

		if (!response_stream || http_version.substr(0, 5) != "HTTP/")
		{
			std::cout << "Invalid response\n";
			return 1;
		}
			
		boost::system::error_code error;
		while (boost::asio::read(socket, response, boost::asio::transfer_at_least(1), error)) {
			std::cout << &response;
		}
		if (error != boost::asio::error::eof)
			throw boost::system::system_error(error);
		
	}
	catch (std::exception& e)
	{
		std::cout << "Exception: " << e.what() << "\n";
	}

	system("pause");
	return 0;
}